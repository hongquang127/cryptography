package cryptography;

import javafx.beans.InvalidationListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.*;

public class Menu implements Initializable {
    private int i;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> list = FXCollections.observableArrayList("Very weak", "Weak", "Alright",
                "Strong", "Extreme");
        strengthComboBox.setItems(list);
        i = 10;
    }
    @FXML
    private ComboBox<String> strengthComboBox;

    @FXML
    void init(ActionEvent event) {
        switch (strengthComboBox.getValue()) {
            case "Very weak":
                i = 5;
                break;
            case "Weak":
                i = 10;
                break;
            case "Alright":
                i = 25;
                break;
            case "Strong":
                i = 50;
                break;
            case "Extreme":
                i = 100;
                break;
        }
    }

    @FXML
    private TextArea inputText;

    @FXML
    private TextArea outputText;

    private Decode dc;
    private String op;

    @FXML
    void decodeText(ActionEvent event) {
        dc  = new Decode(i);
        op = dc.decode(new StringBuilder(inputText.getText()));
        outputText.setText(op);
    }

    @FXML
    void encode(ActionEvent event) {
        dc = new Decode(i);
        op = dc.encode(new StringBuilder(inputText.getText()));
        outputText.setText(op);
    }

}
