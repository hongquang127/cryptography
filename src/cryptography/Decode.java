package cryptography;

import cryptography.util.Shuffle;
import cryptography.util.SimpleSub;
import cryptography.util.ngram.NgramScore;

import java.util.Random;
import java.util.regex.Pattern;

public class Decode {
    private static StringBuilder replaceAll(String sb, String find, String replace){
        return new StringBuilder(Pattern.compile(find).matcher(sb).replaceAll(replace));
    }

    private int iteration;

    public Decode(int iteration) {
        this.iteration = iteration;
    }

    private char[] addPunctuation(char[] cipherTextArray, int[] specialCharIndexArray , String codedText) {
        int counter1 = 0;
        int counter2 = 0;
        int counter3 = 0;

        // give values to specialCharIndexArray
        for (int q = 0; q < cipherTextArray.length; q++) {
            if ( !Character.isLetter(cipherTextArray[q]) ) {
                specialCharIndexArray[counter1++] = q;
            }
        }

        // replace chars in cipher text with plain text
        // without replacing special characters
        for (int j = 0; j < cipherTextArray.length; j++) {
            if ( j == specialCharIndexArray[counter2] && j != 0) {
                counter2++;
                continue;
            }
            System.out.println(j + " - " + counter3);
            cipherTextArray[j] = codedText.toCharArray()[counter3++];
        }

        return  cipherTextArray;
    }

    String encode(StringBuilder plainText) {
        String codedText;

        char[] cipherTextArray = plainText.toString().toUpperCase().toCharArray(); // clone array of cipher text
        int[] specialCharIndexArray = new int[plainText.toString().length()];      // hold index of special char

        plainText = replaceAll(plainText.toString().toUpperCase(), "[^A-Z]+","");

        StringBuilder key = Shuffle.shuffle(new StringBuilder("AJPCZWRLFBDKOTYUQGENHXMIVS"));
        codedText =  new SimpleSub(key).encipher(plainText).toString();

        return new String(addPunctuation(cipherTextArray, specialCharIndexArray, codedText));
    }

    String decode(StringBuilder cipherText) {
        String decodedText = "";

        NgramScore fitness = new NgramScore();
        char[] cipherTextArray = cipherText.toString().toUpperCase().toCharArray(); // clone array of cipher text
        int[] specialCharIndexArray = new int[cipherText.toString().length()];      // hold index of special char


        cipherText = replaceAll(cipherText.toString().toUpperCase(), "[^A-Z]+","");

        StringBuilder maxKey = new StringBuilder("ABCDEFGHIJKLMNOPQRSTUVWXYZ");

        double maxScore = -99000000, parentScore;
        StringBuilder parentKey = maxKey;

        int i = 0;
        while (i < iteration) {
            i = i+1;
            parentKey = Shuffle.shuffle(parentKey);
            StringBuilder deciphered = new SimpleSub(parentKey).decipher(cipherText);
            parentScore = fitness.score(deciphered.toString());

            int count = 0;

            while (count<1000) {
                Random rand = new Random();
                int a = rand.nextInt(26);
                int b = rand.nextInt(26);
                StringBuilder child = new StringBuilder(parentKey.toString());
                child.setCharAt(a, parentKey.charAt(b));
                child.setCharAt(b, parentKey.charAt(a));

                deciphered = new SimpleSub(child).decipher(cipherText);
                double score = fitness.score(deciphered.toString());
                if (score>parentScore) {
                    parentScore = score;
                    parentKey = child;
                    count = 0;
                }
                count = count + 1;
            }
            if (parentScore > maxScore) {
                maxScore = parentScore;
                maxKey = parentKey;
                System.out.println("\n Best score so far: " + maxScore + " on iteration " + i);
                SimpleSub ss = new SimpleSub(maxKey);
                System.out.println("	Best key: " + maxKey);
                decodedText = ss.decipher(cipherText).toString();
                System.out.println("	Plaintext: " + ss.decipher(cipherText));
            }
        }

        System.out.println("\nEND.");

        System.out.println(cipherTextArray);
        System.out.println(specialCharIndexArray);
        System.out.println(decodedText);
        return new String(addPunctuation(cipherTextArray, specialCharIndexArray, decodedText));
    }
}