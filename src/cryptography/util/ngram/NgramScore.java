package cryptography.util.ngram;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class NgramScore {
	private Map<String, Double> ngrams;
	private int length;
	private long sum;
	
	public NgramScore() {
		super();
		ngrams = new HashMap<String, Double>();
		Path currentRelativePath = Paths.get("");
		String s = currentRelativePath.toAbsolutePath().toString();
		System.out.println(s + "\\src\\cryptography\\util\\ngram\\english_quadgrams.txt");

		String filename = s + "\\src\\cryptography\\util\\ngram\\english_quadgrams.txt";
		// length of quadgrams
		length = 4;
		sum = 0;
		// Read file line by line
		try (Stream<String> lines = Files.lines(Paths.get(filename), Charset.defaultCharset())) {
			lines.forEachOrdered(line -> split(line));
		} catch (IOException e) {
			e.printStackTrace();
		}

		ngrams.replaceAll((key,value) -> Math.log10(value/sum));

//		System.out.print("\t" + ngrams);
		floor = Math.log10(0.01/sum);

	}

	private double floor;

	// Split line to get first 4 letters and its corresponding value
	private void split(String line) {
		String[] map = line.split(" ");
//		System.out.println("output string: " + map[1]);
		double value = Integer.valueOf(map[1]);
		sum += value;
		ngrams.put(map[0], value);
	}

	public double score(String text) {
		double score = 0;
		for (int i = 0; i<text.length()-length+1; i++) {
			if (ngrams.containsKey((text.substring(i, i+4)))) {
				score += ngrams.get(text.substring(i, i+4));
			}
			else score +=floor;
		}
		//System.out.println("Score: " + score);

		return score;
	}
}
